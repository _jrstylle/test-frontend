# Teste Frontend to Akna company

This project uses ES6, Webpack, VeuJS and json-server.

Maybe you want to read about them:
- [VueJS](https://vuejs.org/v2/guide/)
- [JSON Server](https://github.com/typicode/json-server#getting-started)
- [Webpack](https://webpack.js.org/)

### Installation

First of all, install the dependencies to run this project.

- [NodeJS](http://nodejs.org/)
- [Vue CLI](https://cli.vuejs.org/guide/)

```sh
# Clone this repository
$ git clone https://_jrstylle@bitbucket.org/akna/test-frontend.git
$ cd test-frontend

# install gulp globally
$ npm install -g @vue/cli

# install dependencies
$ npm install

```

With the commands above, you have everything to start.

```sh
├── README.md
├── db.json
├── babel.config.js
├── package.json
├── package-lock.json
└── src
    ├── main.js
    ├── App.vue
    ├── components
    │   └── BookForm.vue
    │   └── TableForm.vue
```

Those folders and file will change during the project.

### Tasks

- `npm run json-server`: run the fake API to VueJS application
- `npm run dev`: compiles and hot-reloads the files to for development
- `npm run build`: compiles and minifies the files to for send VueJS application to production

is interesting you run the json-server and dev tasks in parallel to see well how to application work


to do this is very easy, you only need open two instances of the terminal in your compute and run the tasks
