import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import BookTable from './components/BookTable.vue';
import BookForm from './components/BookForm.vue';

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: "/",
      name: "books",
      component: BookTable,
      props: true
    },
    {
      path: "/edit/:book",
      name: "editBook",
      component: BookForm,
      props: true
    },
    {
      path: "/add",
      name: "addBook",
      component: BookForm,
    }
  ]
})



new Vue({
  router,
  render: h => h(App),
}).$mount('#bookstoreApp')
